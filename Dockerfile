from fedora
workdir /build
copy build-setup .
run ./build-setup
copy . .
cmd ./convert-docker-to-qcow ubuntu
